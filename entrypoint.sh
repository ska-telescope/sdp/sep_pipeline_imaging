#!/usr/bin/env bash
set -Eeo pipefail
# TODO swap to -Eeuo pipefail above (after handling all potentially-unset variables)

# kickoff
if [ "$1" = 'imaging' ]; then
	# launch app
	cd /app
	exec ./imaging "${@:2}" # masks away $1 from input to program
fi

exec "$@"
