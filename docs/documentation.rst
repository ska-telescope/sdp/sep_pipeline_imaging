*************
Documentation
*************

Overview
========

Describe the imaging pipeline, what it is, etc.

----

Dependencies
============

Hardware
--------
An NVIDIA GPU capable of building and executing CUDA applications

Software
--------

| CUDA Runtime
| NVCC
| Kubernetes installation (elaborate)
| Libfyaml
| Sphinx
| ReadTheDocs
| Breathe
| Go
| Cmake
| Git
| more to come...

----


Configuration of the Pipeline
=============================

Configuration of the SEP Imaging Pipeline is to be performed prior to pipeline execution. The pipeline has been designed to operate dynamically, such that a pipeline configuration can be defined exclusive of the source code (that is, it does not need to be recompiled for differing configurations). The pipeline utilizes two YAML documents for managing configurations of the pipeline, which can be found under the ``imaging_configs`` folder at the root directory of the project.

The first file is the ``defaults.yaml`` file, which supplies default configuration values to the SEP Imaging Pipeline. It is a requirement that this file exists, and contains a default entry for each configurable attribute. Removing attributes from this file will result in the pipeline refusing to execute, so do not remove any attributes. However, the attribute default values can be customized to your needs. This file is useful if you wish to perform multiple executions of the pipeline using identical configurations, with slight variations using the custom override file described in the next paragraph. Below is an example of the default config file used in the SEP Imaging Pipeline, which has been appropriately configured for the GLEAM Small dataset.

| --- 
| # General 
| NUM_RECEIVERS:                     512
| GRID_SIZE:                         2048
| GRID_PADDING_SCALAR:               1.2
| CELL_SIZE:                         8.52211548825356E-06
| FREQUENCY_HZ:                      299792458.0
| VIS_INTENSITY_FILE:                "GLEAM_small_subset_intensity.csv" 
| VIS_UVW_FILE:                      "GLEAM_small_subset_uvw.csv" 
| INPUT_DATA_PATH:                   "../../imaging_data/input/" 
| OUTPUT_DATA_PATH:                  "../../imaging_data/output/" 
| NUM_MAJOR_CYCLES:                  1 
| RIGHT_ASCENSION:                   1 
| SAVE_DIRTY_IMAGES:                 1 
| SAVE_RESIDUAL_IMAGES:              0 
| SAVE_EXTRACTED_SOURCES:            0 
| SAVE_PREDICTED_VIS:                0 
| SAVE_ESTIMATED_GAINS:              0 
| # GPU Specific 
| RETAIN_DEVICE_MEM:                 1 
| # Gain Calibration
| PERFORM_GAIN_CALIBRATION:          0 
| MAX_CALIBRATION_CYCLES:            5 
| NUM_OF_CALIBRATION_CYCLES:         1 
| USE_DEFAULT_GAINS:                 1 
| DEFAULT_GAINS_FILE:                "default_gains.csv"
| # W-Projection Gridding 
| MIN_HALF_SUPPORT:                  4 
| MAX_HALF_SUPPORT:                  4 
| NUM_W_PLANES:                      17 
| OVERSAMPLING:                      4 # must be power of 2
| MAX_W_TERM:                        1895.410847844 
| LOAD_KERNELS_FROM_FILE:            0 
| SAVE_KERNELS_TO_FILE:              0 
| # Deconvolution 
| NUM_MINOR_CYCLES_CALIBRATION:      400 
| NUM_MINOR_CYCLES_IMAGING:          100 
| LOOP_GAIN:                         0.1 
| WEAK_SOURCE_THRESHOLD_CALIBRATION: 0.005 
| WEAK_SOURCE_THRESHOLD_IMAGING:     0.0002 
| NOISE_FACTOR:                      1.2 



As noted earlier, the second file is a custom override file, named ``custom.yaml``. The purpose of this file is to apply overrides to the configuration of the pipeline after the default configuration has been applied. Also noted earlier, this is useful if you wish to run multiple executions of this software with the same defaults, but variable diffences in custom configuration (ie: increased number of w-planes, greater oversampling, etc.). Unlike the defaults file, this custom file does not have to explicitly restate all configuration attributes. You will only need to supply the attributes you wish to override from their default values. See below for an example of a custom configuration:

| ---
| # Add custom overrides here
| NUM_W_PLANES: 50
| OVERSAMPLING: 16

----


Building and Executing
======================

How to build the imaging pipeline (dependencies and libraries, etc.)

Stand-alone
-----------
| # To build and compile the pipeline on the local machine
| $ ./pipeline.sh build
| # To execute the pipeline on the local machine
| $ ./pipeline.sh run
| # To clean up after execution (deletes build folder)
| $ ./pipeline.sh clean

Kubernetes
----------
| # Ensure fresh build of Docker Image
| $ make image
| # Push image to local registry
| $ make push
| # NOTE: Customize configs before the following step is performed
| # To execute as one-off job
| $ make redeploy
| # To get output logs from completed job
| $ make logs

----

The Controller
==============

Description of the controller which glues the system together and orchestrates the execution of the pipeline.

Host Functions
--------------

.. doxygenfunction:: execute_controller

.. doxygenfunction:: pretend_imaging_pipeline

.. doxygenfunction:: populate_memory_megion

.. doxygenfunction:: load_vis_into_memory_regions

.. doxygenfunction:: clean_up

.. doxygenfunction:: exit_and_host_clean

.. doxygenfunction:: save_image_to_file

.. doxygenfunction:: init_config_from_file

.. doxygenfunction:: init_config

.. doxygenfunction:: init_mem

.. doxygenfunction:: kernel_host_setup

.. doxygenfunction:: visibility_host_setup

.. doxygenfunction:: visibility_UVW_host_setup

.. doxygenfunction:: visibility_intensity_host_setup

.. doxygenfunction:: allocate_host_images

.. doxygenfunction:: gains_host_set_up

.. doxygenfunction:: calculate_receiver_pairs

.. doxygenfunction:: correction_set_up

.. doxygenfunction:: create_1D_half_prolate

.. doxygenfunction:: save_predicted_visibilities

.. doxygenfunction:: save_extracted_sources

.. doxygenfunction:: parse_config_attribute

.. doxygenfunction:: parse_config_attribute_bool

.. doxygenfunction:: update_calculated_configs

CUDA Kernels
------------

Does not currently utilize GPU acceleration.

----


W-Projection Convolution Kernels
================================
Generator code for creating a set of W-Projection convolutional kernels for use within the Convolutional Gridding phase of the SEP Imaging Pipeline.

Host Functions
--------------

.. doxygenfunction:: generate_w_projection_kernels

.. doxygenfunction:: generate_phase_screen

.. doxygenfunction:: normalize_kernels_by_maximum

.. doxygenfunction:: normalize_kernels_sum_of_one

.. doxygenfunction:: calculate_support

.. doxygenfunction:: get_next_pow_2

.. doxygenfunction:: populate_ps_window

.. doxygenfunction:: calculate_window_stride

.. doxygenfunction:: prolate_spheroidal

.. doxygenfunction:: load_kernels_from_file

.. doxygenfunction:: are_kernel_files_available

.. doxygenfunction:: save_kernels_to_file

.. doxygenfunction:: bind_kernels_to_host

CUDA Kernels
------------

Does not currently utilize GPU acceleration.


Convolutional Gridding
======================

Description of the W-Projection based convolutional gridding module.

Host Functions
--------------
.. doxygenfunction:: gridding_set_up

.. doxygenfunction:: gridding_run

.. doxygenfunction:: psf_normalization

.. doxygenfunction:: fft_run

.. doxygenfunction:: gridding_set_up

.. doxygenfunction:: convolution_correction_run

.. doxygenfunction:: gridding_memory_transfer

.. doxygenfunction:: psf_memory_transfer

.. doxygenfunction:: gridding_clean_up

.. doxygenfunction:: gridding_execute

.. doxygenfunction:: psf_execute

.. doxygenfunction:: copy_kernels_to_device

.. doxygenfunction:: copy_visibilities_to_device


CUDA Kernels
------------

.. doxygenfunction:: gridding

.. doxygenfunction:: find_psf_max

.. doxygenfunction:: psf_normalization_kernel

.. doxygenfunction:: complex_mult

.. doxygenfunction:: fft_shift_complex_to_complex

.. doxygenfunction:: fft_shift_complex_to_real

.. doxygenfunction:: execute_convolution_correction

----

Fast Fourier Transform (FFT)
============================

Host Functions
--------------

.. doxygenfunction:: fft_2d

.. doxygenfunction:: fft_shift_2d

.. doxygenfunction:: calc_bit_reverse_indices

CUDA Kernels
------------

Does not currently utilize GPU acceleration.

----

Convolution Correction
======================

Host Functions
--------------

CUDA Kernels
------------

----

Deconvolution (Högbom CLEAN)
============================

Host Functions
--------------

.. doxygenfunction:: deconvolution_execute

.. doxygenfunction:: deconvolution_set_up

.. doxygenfunction:: deconvolution_run

.. doxygenfunction:: deconvolution_memory_transfer

.. doxygenfunction:: deconvolution_clean_up

.. doxygenfunction:: copy_psf_to_device

.. doxygenfunction:: allocate_device_maximums

.. doxygenfunction:: copy_sources_to_host

.. doxygenfunction:: copy_sources_to_device

.. doxygenfunction:: save_sources_to_file

.. doxygenfunction:: point_spread_function_free

.. doxygenfunction:: local_maximums_free

CUDA Kernels
------------

.. doxygenfunction:: grid_to_image_coords_conversion

.. doxygenfunction:: image_to_grid_coords_conversion

.. doxygenfunction:: scale_dirty_image_by_psf

.. doxygenfunction:: find_max_source_row_reduction

.. doxygenfunction:: find_max_source_col_reduction

.. doxygenfunction:: compress_sources

.. doxygenfunction:: subtract_psf_from_image

----

Direct Fourier Transform (DFT)
==============================

Host Functions
--------------

.. doxygenfunction:: dft_execute

.. doxygenfunction:: dft_set_up

.. doxygenfunction:: dft_run

.. doxygenfunction:: dft_memory_transfer

.. doxygenfunction:: dft_clean_up

CUDA Kernels
------------

.. doxygenfunction:: direct_fourier_transform

----

Gain Calibration
================

Host Functions
--------------

.. doxygenfunction:: gains_apply_execute

.. doxygenfunction:: gain_calibration_execute

.. doxygenfunction:: gains_set_up

.. doxygenfunction:: gains_apply_run

.. doxygenfunction:: gains_apply_memory_transfer

.. doxygenfunction:: gain_calibration_memory_transfer

.. doxygenfunction:: gains_clean_up

.. doxygenfunction:: gain_calibration_run

.. doxygenfunction:: execute_calibration_SVD

.. doxygenfunction:: check_cuda_solver_error_aux

.. doxygenfunction:: execute_gains

.. doxygenfunction:: rotateAndOutputGains

.. doxygenfunction:: update_gains

.. doxygenfunction:: free_device_gains

.. doxygenfunction:: free_device_receiver_pairs

.. doxygenfunction:: copy_gains_to_host

.. doxygenfunction:: copy_gains_to_device

.. doxygenfunction:: copy_receiver_pairs_to_device

CUDA Kernels
------------

.. doxygenfunction:: apply_gains_subtraction

.. doxygenfunction:: reset_gains

.. doxygenfunction:: update_gain_calibration

.. doxygenfunction:: gains_product

.. doxygenfunction:: calculate_suq_product

.. doxygenfunction:: calculate_delta_update_gains

.. doxygenfunction:: apply_gains

.. doxygenfunction:: calibrate_gains

.. doxygenfunction:: reciprocal_transform

.. doxygenfunction:: complex_multiply

.. doxygenfunction:: complex_divide

.. doxygenfunction:: complex_subtract

.. doxygenfunction:: complex_reciprocal

.. doxygenfunction:: flip_for_i

.. doxygenfunction:: flip_for_neg_i

.. doxygenfunction:: complex_conjugate

----

Common
======

Host Functions
--------------

.. doxygenfunction:: check_cuda_error_aux

.. doxygenfunction:: cufft_safe_call

.. doxygenfunction:: cuda_get_error_enum

.. doxygenfunction:: image_free

.. doxygenfunction:: allocate_new_image_memory

.. doxygenfunction:: allocate_grid_device_mem

.. doxygenfunction:: copy_gpu_image_to_host

.. doxygenfunction:: allocate_device_measured_vis

.. doxygenfunction:: allocate_device_vis_coords

.. doxygenfunction:: free_device_vis_coords

.. doxygenfunction:: free_device_measured_vis

.. doxygenfunction:: free_device_predicted_vis

.. doxygenfunction:: copy_predicted_vis_to_host

.. doxygenfunction:: copy_measured_vis_to_device

.. doxygenfunction:: copy_predicted_vis_to_device

.. doxygenfunction:: copy_vis_coords_to_device

.. doxygenfunction:: save_predicted_vis_to_file

.. doxygenfunction:: free_device_sources

.. doxygenfunction:: allocate_device_sources

.. doxygenfunction:: validate_snprintf

CUDA Kernels
------------

Does not currently utilize GPU acceleration.

----

Timing
======

Host Functions
--------------

.. doxygenfunction:: start_timer

.. doxygenfunction:: stop_timer

CUDA Kernels
------------

Does not currently utilize GPU acceleration.

----

Custom Macros
=============

.. doxygenfunction:: MIN

.. doxygenfunction:: MAX

.. doxygenfunction:: CUDA_SOLVER_CHECK_RETURN

.. doxygenfunction:: CUDA_CHECK_RETURN

.. doxygenfunction:: CUFFT_SAFE_CALL

.. doxygenfunction:: snprintf


Custom Structures
=================

.. doxygenstruct:: Complex
    :members:

.. doxygenstruct:: Visibility
    :members:

.. doxygenstruct:: Source
    :members:

.. doxygenstruct:: Config
    :members:

.. doxygenstruct:: MemoryRegions
    :members:

.. doxygenstruct:: Timer
    :members:

.. doxygenstruct:: Timings
    :members:

.. doxygenstruct:: Host_Mem_Handles
    :members:

.. doxygenstruct:: Device_Mem_Handles
    :members:

Custom Definitions
==================

.. doxygendefine:: SPEED_OF_LIGHT

.. doxygendefine:: SINGLE_PRECISION

.. doxygendefine:: MAX_LEN_CHAR_BUFF
