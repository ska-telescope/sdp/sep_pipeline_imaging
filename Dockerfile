ARG NVIDIA_BASE_IMAGE=nvidia/cuda:10.1-devel
ARG NVIDIA_RUNTIME_IMAGE=nvidia/cuda:10.1-runtime

# vanilla Ubuntu image used as builder
FROM $NVIDIA_BASE_IMAGE AS build

LABEL \
      author="Adam Campbell <adamrobcampbell@gmail.com>, Seth Hall <seth.hall@aut.ac.nz>, Andrew Ensor <aensor@aut.ac.nz>" \
      description="SEP Pipeline - Imaging" \
      license="BSD-3-Clause" \
      registry="library/NZAPP/SEP_Pipeline_Imaging" \
      vendor="None" \
      org.skatelescope.team="NZAPP" \
      org.skatelescope.version="0.0.1" \
      org.skatelescope.website="https://gitlab.com/ska-telescope/sep_pipeline_imaging"

# Disable prompts from apt.
ENV DEBIAN_FRONTEND noninteractive

# just show what CUDA runfile we are using
RUN \
    echo "Building with: $CUDA_VERSION"

# now install dependencies for CUDA runfile install
RUN apt-get update && \
    apt-get  -yq --no-install-recommends install \
      clang-8 \
      cmake \
      git \
      gcc \
      autoconf \
      automake \
      libtool \
      make \
      libltdl-dev \
      pkg-config \
      libyaml-dev \
      check \
      jq \
      wget \
      && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /var/cache/apt/archives/*

# Download latest version of libfyaml from github (not the best way to go about this...)
RUN wget -q https://github.com/pantoniou/libfyaml/releases/download/v0.5.4/libfyaml-0.5.4.tar.gz

# Install libfyaml and clean up
RUN tar -zxf libfyaml-0.5.4.tar.gz \
      && cd libfyaml-0.5.4 \
      && ./bootstrap.sh \
      && ./configure \
      && make \
#       && make check \
      && make install \
      && cd .. \
      && rm -rf libfyaml-0.5.4 \
      && rm libfyaml-0.5.4.tar.gz

RUN \
    mkdir -p /app

# copy over project ready for build
COPY main.cpp test.cpp Makefile CMakeLists.txt /app/
COPY imaging/ /app/imaging/
COPY .git /app/.git/

# build imaging pipeline application
RUN \
    cd /app && make build -j8

# Now - create a clean image without build environment
FROM $NVIDIA_RUNTIME_IMAGE

# copy in built imaging pipeline app
COPY --from=build /app/build/imaging/imaging /app/imaging

# copy in boot strap shellscript that does ldconfig and runs imaging pipeline
COPY entrypoint.sh /entrypoint.sh

# Setup the entrypoint or environment
ENTRYPOINT ["/entrypoint.sh"]

# Run - default is imaging_pipeline
CMD ["imaging"]
# add option for calling system test from here

# vim:set ft=dockerfile:
