#!/bin/sh

set -x

sudo minikube stop
sudo minikube delete
# sudo kubeadm reset --force # NO KUBEADM INSTALLED

sudo rm -rf ${HOME}/.kube
sudo rm -rf /root/.minikube
sudo rm -rf /var/lib/kubeadm.yaml
sudo rm -rf /data/minikube
sudo rm -rf /var/lib/minikube
sudo rm -rf /var/lib/kubelet
sudo rm -rf /etc/kubernetes
sudo rm -rf /etc/cni
sudo rm -rf /opt/cni
sudo rm -rf /var/lib/calico
sudo rm -rf /etc/systemd/system/kubelet.service.d

sudo systemctl daemon-reload
sudo systemctl stop kubelet

sudo systemctl stop crio
sudo systemctl disable crio

sudo systemctl restart containerd
sudo systemctl restart docker

# sudo apt purge kubelet -y # NO KUBELET INSTALLED
sudo iptables -F && sudo iptables -t nat -F && sudo iptables -t mangle -F && sudo iptables -X

sudo systemctl restart docker

