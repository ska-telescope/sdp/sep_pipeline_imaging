#!/bin/sh

sudo systemctl stop cri-o
sudo systemctl stop crio
sudo systemctl restart docker

sudo minikube start \
    --wait-timeout=10m0s \
    --extra-config=kubelet.resolv-conf=/run/systemd/resolve/resolv.conf \
        --extra-config=kubelet.network-plugin=cni \
        --extra-config=kubeadm.pod-network-cidr=10.200.0.0/16 \
        --network-plugin=cni \
    --vm-driver=none --container-runtime=containerd

sudo chown -R $USER $HOME/.minikube $HOME/.kube
sudo chgrp -R $USER $HOME/.minikube $HOME/.kube

export KUBECONFIG=$HOME/.kube/config

sleep 5
curl -O -L https://docs.projectcalico.org/v3.11/manifests/calico.yaml
POD_CIDR="10.200.0.0/16" \
	sed -i -e "s?192.168.0.0/16?$POD_CIDR?g" calico.yaml
kubectl apply -f calico.yaml

# see https://github.com/projectcalico/calico/issues/2904
kubectl -n kube-system set env daemonset/calico-node FELIX_XDPENABLED=false
kubectl -n kube-system set env daemonset/calico-node CALICO_IPV4POOL_CIDR=10.200.0.0/16
kubectl -n kube-system set env daemonset/calico-node IP_AUTODETECTION_METHOD=interface=enp.*

grep etcd_endpoints calico.yaml

sudo minikube addons enable ingress

kubectl taint nodes threadrippergpu  node.kubernetes.io/disk-pressure:NoSchedule-

docker start registry

# fix containerd

sudo bash -c 'cat <<EOF >/etc/containerd/config.toml
root = "/var/lib/containerd"
state = "/run/containerd"
oom_score = 0

[grpc]
  address = "/run/containerd/containerd.sock"
  uid = 0
  gid = 0
  max_recv_message_size = 16777216
  max_send_message_size = 16777216

[debug]
  address = ""
  uid = 0
  gid = 0
  level = ""

[metrics]
  address = ""
  grpc_histogram = false

[cgroup]
  path = ""

[plugins]
  [plugins.cgroups]
    no_prometheus = false
  [plugins.cri]
    stream_server_address = ""
    stream_server_port = "10010"
    enable_selinux = false
    sandbox_image = "k8s.gcr.io/pause:3.1"
    stats_collect_period = 10
    systemd_cgroup = false
    enable_tls_streaming = false
    max_container_log_line_size = 16384
    [plugins.cri.containerd]
      snapshotter = "overlayfs"
      no_pivot = true
      [plugins.cri.containerd.default_runtime]
        runtime_type = "io.containerd.runtime.v1.linux"
        runtime_engine = ""
        runtime_root = ""
      [plugins.cri.containerd.untrusted_workload_runtime]
        runtime_type = ""
        runtime_engine = ""
        runtime_root = ""
      [plugins.cri.containerd.runtimes.nvidia]
        runtime_type = "io.containerd.runtime.v1.linux"
        runtime_engine = "/usr/bin/nvidia-container-runtime"
        runtime_root = ""
    [plugins.cri.cni]
      bin_dir = "/opt/cni/bin"
      conf_dir = "/etc/cni/net.d"
      conf_template = ""
    [plugins.cri.registry]
      [plugins.cri.registry.mirrors]
        [plugins.cri.registry.mirrors."docker.io"]
          endpoint = ["https://registry-1.docker.io"]
  [plugins.diff-service]
    default = ["walking"]
  [plugins.linux]
    shim = "containerd-shim"
    runtime = "runc"
    runtime_root = ""
    no_shim = false
    shim_debug = false
  [plugins.scheduler]
    pause_threshold = 0.02
    deletion_threshold = 0
    mutation_threshold = 100
    schedule_delay = "0s"
    startup_delay = "100ms"
EOF'

sudo systemctl restart containerd

cat <<EOF | kubectl apply -f -
apiVersion: node.k8s.io/v1beta1
kind: RuntimeClass
metadata:
  name: nvidia
handler: nvidia
EOF

