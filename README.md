# SKA-SDP-cuda-imaging-pipeline

 

## Overview

 

The SKA-SDP-cuda-imaging-pipeline is a CUDA/C++ implementation of an imaging/calibration pipeline. It was initially created by the HPC Research Laboratory at AUT for the NZAPP team as a prototype GPU-accelerated SKA pipeline. It is freely available under a BSD 3-clause license for use or modification, and contributions or feedback are encouraged.

 

### Implemented stages

* W-projection kernel generation.

* Gain calibration application and calculation of residual visibilities.

* Gridding.

* Fourier transform (using cuFFT).

* Deconvolution (using basic Hogbom cleaning).

* Direct fourier transform to predict visibilities (instead can alternatively use a degridding stage, but this stage is not currently maintained).

* Gain calibration (using SVD).

 

### Features

* Each stage is implemented in its own independent modular codebase, with a controller (`controller.cu`) configured by YAML that allows them to be plugged together as a functional pipeline.

* W-projection kernels can either be generated on the fly or saved/read via file (not currently GPU accelerated).

* Configurable multiple major cycles of gain calibration and/or imaging of residuals can be used.

* Visibilities can be either transferred back to main memory between stages (`LOW_SPEC`) for platforms that either have limited GPU device memory or want to debug individual stages, or else held on GPU device memory (`HIGH_SPEC`) without copy transfers for improved performance.

* Visibility weighting schemes can be specified (in progress during PI#9).

* Pipeline configurations are specified via YAML.

* Input visibilities can be ingested/predicted/stored as either complex 16+16-bit half precision floating point values or else complex 32+32-bit single precision (specified via the `ENABLE_16BIT_VISIBILITIES` compiler directive in `common.h`).

* Processing calculations can be either in single precision or double precision (specified via the `SINGLE_PRECISION` compiler directive in `common.h`).

 

---

 

## Quick Start Guide

 

1. Start with a platform that has an Nvidia GPU (required for CUDA support), with CMake, GCC/G++, and the CUDA toolkit installed (see Software and hardware requirements below).

1. The project can be obtained from the GitLab SKA telescope repository at `https://gitlab.com/ska-telescope`:

    ```bash
    git clone https://gitlab.com/ska-telescope/sep_pipeline_imaging.git
    ```

    Note the project name will soon be modified to ska-sdp-cuda-imaging-pipeline.

1. Optionally, the `common.h` header file in the `imaging` code folder can be edited to modify compiler directives such as `SINGLE_PRECISION` (default `1` for using single precision calculations or `0` for using double precision calculations) and `ENABLE_16BIT_VISIBILITIES` (default `0` for 32-bit visibilities or `1` for 16-bit visibilities).

1. The project should be (optionally cleaned and) built initially and whenever compiler directives are modified, by using the `pipeline.sh` shell script found in the main project folder:

    ```bash
    ./pipeline.sh clean
    ./pipeline.sh build
    ```

1. Visibility files (with suffix `.vis`) and their corresponding uvw coordinate files (with suffix `.uvw`) should be placed in the `imaging_data/input` folder. The first entry in each data file must specify how many data it holds (number of complex values for visibility files or number of uvw coordinates for uvw coordinate files). For convenience NZAPP provide a python script `casa_visibility_extract.py` for producing these files from a measurement set within CASA. The script is available where???. Note that the half or single precision of the visibilities used by the script must match that specified by the `ENABLE_16BIT_VISIBILITIES` compiler directive. Optionally, initial antenna gains can also be placed in this folder as a csv file.

1. The folder `imaging-config` contains the YAML configuration files `default.yaml` (holding default configurations for the pipeline) and `custom.yaml` (for overriding any default configurations). For new users it is suggested to only initially edit `custom.yaml` to override default configurations that need be changed (such as `VIS_INTENSITY_FILE` and `VIS_UVW_FILE` to specify input datafile names), and only later modify `default.yaml` to specify configurations that will change infrequently.

1. The pipeline can then be run to process the visibility data:

    ```bash
    ./pipeline.sh run
    ```

    This results in one or more output files being generated in the `imaging_data/output` folder.

1. For convenience NZAPP provide a python script for visualising and comparing resulting image files (requires the `matplotlib`, `numpy`, `pandas` python libraries). Sample scripts are available where???

 

---
# Future work and improvements
+ In-depth testing and validation of multiple major cycle functionality
+ Revisit the LOW_SPEC set-up, transfer, execute, copy-back, and destroy model for improved code standards
+ Investigate the use of UV-plane padding to correct for artifacts on the edge of synthesized images (note that the UV-plane should be approximately 1.2x the size of the desired image resolution).
+ Implement suitable clipping functionality for dealing with padded UV-planes.
+ Implement suitable functionality for reading and writing relevant data (visibilities, kernels, images, etc.) in binary format, as opposed to the current CSV standard.
+ Implement isolated module for generating point spread function image for use in overall pipeline. Currently only uses point spread function image from CSV file.
+ Implement W-Projection kernel creation functionality via CUDA kernel for improved performance.
+ Implement functionality to load in default gains from file, as opposed to using an array of fixed complex gains (1.0+0.0i)
---
#### Software and hardware requirements
1. CMake (version >= 3.10.2)
2. GCC/G++ (version >= 7.4.0)
3. CUDA Toolkit (version >= 10.1) **(Required for CUDA SVD functionality)**
4. Valgrind (version >= 3.13.0)
5. Docker (version >= 19.03.5)
6. An NVIDIA GPU which supports modern compute capability (6.0 or above)
##### **(*Note: this versioning set up may be flexible. However, this was the configuration used during prototype development*)**
---
##### Instructions for installation of this software (includes profiling, linting, building, and unit testing):
1. Ensure you have an NVIDIA based GPU (**mandatory!**)
2. Install the [CUDA](https://developer.nvidia.com/cuda-downloads) toolkit and runtime (refer to link for download/installation procedure)
3. Install [Valgrind](http://valgrind.org/) (profiling, memory checks, memory leaks etc.)
   ```bash
   $ sudo apt install valgrind
   ```
4. Install [Cmake](https://cmake.org/)/[Makefile](https://www.gnu.org/software/make/) (build tools)
   ```bash
   $ sudo apt install cmake
   ```
6. Install [Cppcheck](http://cppcheck.sourceforge.net/) (linting)
   ```bash
   $ sudo apt install cppcheck
   ```
7. Configure the code for usage (**modify pipeline config [controller.cu]**)
8. Create local execution folder
    ```bash
   $ mkdir build && cd build
   ```
9. Build gridder project (from project folder)
   ```bash
   $ cmake .. -DCMAKE_BUILD_TYPE=Release && make
   ```
10. **Important: set -CDMAKE_BUILD_TYPE=Debug if planning to run Valgrind. Debug mode disables compiler optimizations, which is required for Valgrind to perform an optimal analysis.**
---
##### Instructions for usage of this software (includes executing, testing, linting, and profiling):
To perform memory checking, memory leak analysis, and profiling using [Valgrind](http://valgrind.org/docs/manual/quick-start.html), execute the following (assumes you are in the appropriate *build* folder (see step 5 above):
```bash
$ valgrind --leak-check=yes -v ./imaging_pipeline
```
To execute linting, execute the following command (assumes you are in the appropriate source code folder):
```bash
$ cppcheck --enable=all your_file_name.file_extension (cpp or cu)
```
To execute the imaging pipeline (once configured and built), execute the following command (also assumes appropriate *build* folder):
```bash
$ ./imaging_pipeline
```
To validate that the imaging pipeline is functioning correctly (system test), execute the following command (again, in *build* folder):
```bash
$ ./system_test
```
**(*Note: required system test data is quite large (~250MB compressed, ~500MB decompressed), and is not included in this repository. It must be downloaded from the following Google Drive address and placed in the root folder of the project (decompressed) in order to perform system testing: [System Test Data](https://drive.google.com/file/d/1latYGMjSvjNnRhLdzc6EBnKVyOr6SCuV/view?usp=sharing)*)**

---
## Running in Containers
### Install Docker onto your system
Firstly, you need to ensure Docker is installed on your system before installing and setting up the NVIDIA Container Runtime. Installation instructions for installing Docker via Terminal can be found on the [Docker website](https://docs.docker.com/install/linux/docker-ce/ubuntu/). Version 19.03.5 was installed during the implementation of this prototype - other versions may be suitable for use. The following is stated on the [Nvidia Container Runtime github page](https://github.com/NVIDIA/nvidia-docker):

> Note that with the release of Docker 19.03, usage of nvidia-docker2 packages are deprecated since NVIDIA GPUs are now natively supported as devices in the Docker runtime.

However, without the nvidia-docker2 packages, the project was unable to be run via docker-compose, as docker-compose would complain that there are no suitable CUDA drivers installed to run the pipeline. This was untrue. Further instructions regarding this will be presented in the next section. 

Furthermore, be sure to install Docker Compose. Instructions for your operating system can be found [here](https://docs.docker.com/compose/install/).
### Install Nvidia Container Runtime
The next step is to install the NVIDIA Container Runtime from the following [github repository](https://github.com/NVIDIA/nvidia-docker). The following is an exerpt from the instructions presented on the aforementioned repository. Note that these instructions assume Ubuntu is the installed OS of choice, and will automatically detect your OS version. Further instructions for other OS are available on the repository:
```bash
$ distribution=$(. /etc/os-release;echo $ID$VERSION_ID)
$ curl -s -L https://nvidia.github.io/nvidia-docker/gpgkey | sudo apt-key add -
$ curl -s -L https://nvidia.github.io/nvidia-docker/$distribution/nvidia-docker.list | sudo tee /etc/apt/sources.list.d/nvidia-docker.list

$ sudo apt-get update && sudo apt-get install -y nvidia-container-toolkit
$ sudo systemctl restart docker
```

The runtime can be tested to ensure the validity of the installation as follows. Note that the following assumes CUDA 10.1 is the version of CUDA on your machine. Moreover, if your login on your system has non-root priveleges, you will need to ensure you execute using *sudo*, or you will need to modify your groupings on your Linux system ([instructions](https://docs.docker.com/install/linux/linux-postinstall/#manage-docker-as-a-non-root-user)).
```bash
$ sudo docker run --gpus all nvidia/cuda:10.1-base nvidia-smi
# or: $ sudo docker run -e NVIDIA_VISIBLE_DEVICES=all nvidia/cuda:10.1-base nvidia-smi
```

Finally, be sure to install the docker container hook:
```bash
# Install nvidia-docker2 and reload the Docker daemon configuration
$ sudo apt-get install -y nvidia-docker2
$ sudo pkill -SIGHUP dockerd
```
### Building the Imaging Pipeline image

Build the image with the following command:
```bash
$ make image # add sudo for root as required
```
This will take some time, as it needs to pull down the large Nvidia docker base images. This is approximately 1.6GB in size.

### Test the Imaging Pipeline image

Ensure you have your relevant imaging data (visibilities, kernels, point spread function, etc.) inside the *data* folder within the root project directory. Ensure you have also built a localized imaging pipeline docker image using the previous step.
```bash
$ make test # add sudo for root as required
```

### Running sample of the Imaging Pipeline via docker-compose
To run the imaging pipeline via docker-compose, make sure you have first built the docker image, and have placed the relevant imaging data into the *data* folder within the root project directory. You can then run the pipeline via docker-compose with the following command:
```bash
$ make up # add sudo for root as required
```

You should observe a similar output as follows (note: your output may differ depending on your dataset and related configurables):

```bash
your-name@your-computer:your/project/path/SEP_Pipeline_Imaging$ make up
IMAGE=sep-pipeline-imaging:latest  RUNTIME=nvidia  docker-compose up
Creating network "sep_pipeline_imaging_default" with the default driver
Creating imaging_pipeline ... done
Attaching to imaging_pipeline
imaging_pipeline    | =========================================================================
imaging_pipeline    | >>> AUT HPC Research Laboratory - SDP Evolutionary Pipeline (Imaging) <<<
imaging_pipeline    | =========================================================================
imaging_pipeline    | UPDATE >>> You are running HIGH-SPEC GPU mode 
imaging_pipeline    | UPDATE >>> Loading kernel support file from ../data/gridder/kernels/w-proj_supports_x16.csv...
imaging_pipeline    | UPDATE >>> Total number of samples needed to store kernels is 108800...
imaging_pipeline    | UPDATE >>> Loading kernel files file from ../data/gridder/kernels/w-proj_kernels_real_x16.csv real and ../data/gridder/kernels/w-proj_kernels_imag_x16.csv imaginary...
imaging_pipeline    | UPDATE >>> Loading visibilities from file ../data/gridder/GLEAM_small_visibilities_corrupted.csv...
imaging_pipeline    | UPDATE >>> Successfully loaded 3924480 visibilities from file...
imaging_pipeline    | UPDATE >>> Setup FFT plan and allocate output device image memory...
imaging_pipeline    | UPDATE >>> PSF read from file with max value to scale grid = 3925689.750000...
imaging_pipeline    | UPDATE >>> Performing major cycle number 0...
imaging_pipeline    | UPDATE >>> Copying measured visibilities to device, number of visibilities: 3924480...
imaging_pipeline    | UPDATE >>> Copying gains array to device, number of num_recievers: 512...
imaging_pipeline    | UPDATE >>> Copying kernels to device, number of samples: 108800...
imaging_pipeline    | UPDATE >>> Copying kernels supports to device, number of planes: 17...
imaging_pipeline    | UPDATE >>> Allocating new UV grid device memory of size 2048 squared complex values...
imaging_pipeline    | UPDATE >>> Gridding using 3833 blocks, 1024 threads, for 3924480 visibilities...
imaging_pipeline    | UPDATE >>> Gridding complete... 
imaging_pipeline    | UPDATE >>> Allocating new image device memory of size 2048 squared real values...
imaging_pipeline    | UPDATE >>> Shifting grid data for FFT...
imaging_pipeline    | UPDATE >>> Performing iFFT...
imaging_pipeline    | UPDATE >>> Shifting grid data back and converting to output image...
imaging_pipeline    | UPDATE >>> FFT COMPLETE...
imaging_pipeline    | UPDATE >>> Saving dirty image to file sample_dirty_image_real.csv... on cycle number 0
imaging_pipeline    | UPDATE >>> Attempting to save image to ../data/output/cycle_0_sample_dirty_image_real.csv... 
imaging_pipeline    | UPDATE >>> Performing deconvolution, up to 500 minor cycles...
imaging_pipeline    | UPDATE >>> Performing minor cycle number: 0...
imaging_pipeline    | UPDATE >>> Performing minor cycle number: 10...
imaging_pipeline    | UPDATE >>> Performing minor cycle number: 20...
imaging_pipeline    | UPDATE >>> Performing minor cycle number: 30...
imaging_pipeline    | UPDATE >>> Performing minor cycle number: 40...
imaging_pipeline    | UPDATE >>> Performing minor cycle number: 50...
imaging_pipeline    | UPDATE >>> Performing minor cycle number: 60...
imaging_pipeline    | UPDATE >>> Performing minor cycle number: 70...
imaging_pipeline    | UPDATE >>> Performing minor cycle number: 80...
imaging_pipeline    | >>> UPDATE: Terminating minor cycles as now just cleaning noise, cycle number 87...
imaging_pipeline    | UPDATE >>> Performing conversion on Source coordinates...
imaging_pipeline    | UPDATE >>> Number of sources found after deconvolution is 30...
imaging_pipeline    | UPDATE >>> Freeing GPU bound local maximums memory...
imaging_pipeline    | UPDATE >>> Executing the Direct Fourier Transform algorithm...
imaging_pipeline    | UPDATE >>> DFT distributed over 3833 blocks, consisting of 1024 threads...
imaging_pipeline    | UPDATE >>> Direct Fourier Transform complete...
imaging_pipeline    | UPDATE >>> Performing gain calibration on this major cycle...
imaging_pipeline    | UPDATE >>> Performing Gain Calibration using 10 calibration cycles...
imaging_pipeline    | UPDATE >>> Performing Gain Calibration cycle: 0 ...
imaging_pipeline    | UPDATE >>> SVD complete...
imaging_pipeline    | UPDATE >>> CALCULATE SUQ PRODUCT... 
imaging_pipeline    | UPDATE >>> CALCULATING DELTA TO UPDATE GAINS ... 
imaging_pipeline    | UPDATE >>> Performing Gain Calibration cycle: 1 ...
imaging_pipeline    | UPDATE >>> SVD complete...
imaging_pipeline    | UPDATE >>> CALCULATE SUQ PRODUCT... 
imaging_pipeline    | UPDATE >>> CALCULATING DELTA TO UPDATE GAINS ... 
imaging_pipeline    | UPDATE >>> Performing Gain Calibration cycle: 2 ...
imaging_pipeline    | UPDATE >>> SVD complete...
imaging_pipeline    | UPDATE >>> CALCULATE SUQ PRODUCT... 
imaging_pipeline    | UPDATE >>> CALCULATING DELTA TO UPDATE GAINS ... 
imaging_pipeline    | UPDATE >>> Performing Gain Calibration cycle: 3 ...
imaging_pipeline    | UPDATE >>> SVD complete...
imaging_pipeline    | UPDATE >>> CALCULATE SUQ PRODUCT... 
imaging_pipeline    | UPDATE >>> CALCULATING DELTA TO UPDATE GAINS ... 
imaging_pipeline    | UPDATE >>> Performing Gain Calibration cycle: 4 ...
imaging_pipeline    | UPDATE >>> SVD complete...
imaging_pipeline    | UPDATE >>> CALCULATE SUQ PRODUCT... 
imaging_pipeline    | UPDATE >>> CALCULATING DELTA TO UPDATE GAINS ... 
imaging_pipeline    | UPDATE >>> Performing Gain Calibration cycle: 5 ...
imaging_pipeline    | UPDATE >>> SVD complete...
imaging_pipeline    | UPDATE >>> CALCULATE SUQ PRODUCT... 
imaging_pipeline    | UPDATE >>> CALCULATING DELTA TO UPDATE GAINS ... 
imaging_pipeline    | UPDATE >>> Performing Gain Calibration cycle: 6 ...
imaging_pipeline    | UPDATE >>> SVD complete...
imaging_pipeline    | UPDATE >>> CALCULATE SUQ PRODUCT... 
imaging_pipeline    | UPDATE >>> CALCULATING DELTA TO UPDATE GAINS ... 
imaging_pipeline    | UPDATE >>> Performing Gain Calibration cycle: 7 ...
imaging_pipeline    | UPDATE >>> SVD complete...
imaging_pipeline    | UPDATE >>> CALCULATE SUQ PRODUCT... 
imaging_pipeline    | UPDATE >>> CALCULATING DELTA TO UPDATE GAINS ... 
imaging_pipeline    | UPDATE >>> Performing Gain Calibration cycle: 8 ...
imaging_pipeline    | UPDATE >>> SVD complete...
imaging_pipeline    | UPDATE >>> CALCULATE SUQ PRODUCT... 
imaging_pipeline    | UPDATE >>> CALCULATING DELTA TO UPDATE GAINS ... 
imaging_pipeline    | UPDATE >>> Performing Gain Calibration cycle: 9 ...
imaging_pipeline    | UPDATE >>> SVD complete...
imaging_pipeline    | UPDATE >>> CALCULATE SUQ PRODUCT... 
imaging_pipeline    | UPDATE >>> CALCULATING DELTA TO UPDATE GAINS ... 
imaging_pipeline    | UPDATE >>>  Gain Calibration Complete ...
imaging_pipeline    | PIPELINE DONE... COPYING GAINS TO HOST
imaging_pipeline    | UPDATE >>> Cleaning up all allocated host memory...
imaging_pipeline    | UPDATE >>> DEALLOCATED THE MEMORY...
imaging_pipeline    | UPDATE >>> Cleaning up all allocated device memory...
imaging_pipeline    | TIMING >>> Gridding elapsed time: 26.157057 milliseconds
imaging_pipeline    | TIMING >>> FFT elapsed time: 0.983200 milliseconds
imaging_pipeline    | TIMING >>> Convolution Correction elapsed time: 0.153600 milliseconds
imaging_pipeline    | TIMING >>> Deconvolution elapsed time: 152.290588 milliseconds
imaging_pipeline    | TIMING >>> Direct Fourier Transform elapsed time: 1.965056 milliseconds
imaging_pipeline    | TIMING >>> Gain Subtraction elapsed time: 0.389184 milliseconds
imaging_pipeline    | UPDATE >>> Imaging Pipeline Complete...
imaging_pipeline exited with code 0
```
To clean up the docker-compose sample of the imaging pipeline, execute the following command:
```bash
$ make down # add sudo for root as required
```

### Running on Kubernetes

Running on Kubernetes can be done in a number of ways, that all boil down to changing the container runtime.  This can be done using Docker, ContainerD, and CRI-O. However, at time of writing the most flexible way that enabled a non-nvidia default runtime to be configured, and use a RunTimeClass configuration for the Pod descriptor, is CRI-O.

In addition to the setup described above for installing the container runtime for Docker, complete the following:

### Install CRI-O

Firstly ensure CRI-O is installed by following the Instructions here: https://kubernetes.io/docs/setup/production-environment/container-runtimes/#cri-o

Enable and start CRI-O using the following command:
```bash
$ sudo systemctl enable crio && sudo systemctl start crio
# Note: if crio fails to start and throws an exception, something similar to the following:
# library config validation: runtime config: invalid conmon path:
# then you need to try starting crio again after modifying the '/etc/crio/crio.conf' file
# to set the path to the conmon binary as: conmon = "/usr/bin/conmon" instead of wherever it points to by default (mine was pointing to conmon = "/usr/libexec/crio/conmon" originally). It would seem that there is a broken path pre-defined in the crio.conf which no longer points to conmon correctly.
# https://github.com/cri-o/cri-o/issues/2903
```

Amend the configuration in `/etc/crio/crio.conf` to be something similar to:
```bash
...
... # existing contents in the crio.conf file will be here
...

  [crio.runtime.runtimes.runc]                 
  runtime_path = "/usr/lib/cri-o-runc/sbin/runc"     
  runtime_type = ""                         

  [crio.runtime.runtimes.nvidia]                     
  runtime_path = "/usr/bin/nvidia-container-runtime"
  runtime_type = ""                                 
  
...

registries = [  
    "docker.io",
    "quay.io",  
]

insecure_registries = ['localhost:5000']
...
... # more existing contents in the crio.conf file will be here
...
```

Lastly, give CRI-O a restart to apply the above conf changes
```bash
$ sudo systemctl restart crio
```

The next step is to install the podman package, which can be done using the following command
```bash
$ sudo apt install podman
```
Once installed, you should configure the registries configuration file (located at `/etc/containers/registries.conf`). By default, the file will be empty, so you can just paste the following text into the configuration file:

```bash
[registries.search]
registries = ['docker.io']

# If you need to access insecure registries, add the registry's fully-qualified name.
# An insecure registry is one that does not have a valid SSL certificate or only does HTTP.
[registries.insecure]
registries = ['localhost:5000']
```

Finally, you will need to install the `crictl` package, which can be done so using the instructions at the following address: https://github.com/kubernetes-sigs/cri-tools/blob/master/docs/crictl.md

### CRI-O Fix Kubernetes Configuration

Kubernetes must be configured to use CRI-O.  For Minikube (which was used for testing at K8s v1.16), this is achieved with something like:
```
sudo minikube start --vm-driver=none --container-runtime=cri-o --extra-config=kubelet.cgroup-driver=systemd
```

# adam continue here - noted issue, the above command seems to override the contents appended to crio.conf at an earlier stage, unsure what to do. File permissions doesnt change anything so not sure at the moment.

### Alternatively using ContainerD

It is possible to use the containerd runtime.  The simplest method of setting this up is currently by following the instructions here: https://kubernetes.io/docs/setup/production-environment/container-runtimes/#containerd .

Once the basic containerd install is complete, the configuration needs to be modified to include the nvidia runtime.

Edit `/etc/containerd/config.toml`, and add the following:
```
...
# after      [plugins.cri.containerd.untrusted_workload_runtime]
      [plugins.cri.containerd.runtimes.nvidia]
        runtime_type = "io.containerd.runtime.v1.linux"
        runtime_engine = "/usr/bin/nvidia-container-runtime"
        runtime_root = ""
...
```

Restart containerd with: `sudo systemctl restart containerd`.  It is best to do this after the Minikube instance has been stopped and deleted.

As for CRI-O ensure that `crictl` is installed as described here: https://github.com/kubernetes-sigs/cri-tools/blob/master/docs/crictl.md

### containerd Fix Kubernetes Configuration

Kubernetes must be configured to use containerd.  For Minikube (which was used for testing at K8s v1.16), this is achieved with something like:
```
sudo minikube start --vm-driver=none --container-runtime=containerd
```


### Define the RunTimeClass

A RunTimeClass is required to direct running Pods towards the `nvidia` runtime.  This is created with something like:
```
cat <<EOF | kubectl apply -f -
apiVersion: node.k8s.io/v1beta1
kind: RuntimeClass
metadata:
  name: nvidia
handler: nvidia
EOF
```

### Run the Helm chart

Once the nvidia integration for Kubernetes is complete, the example Helm chart can be launched with:
```
$ make deploy
ubectl describe namespace "default" || kubectl create namespace "default"
Name:         default
Labels:       <none>
Annotations:  <none>
Status:       Active

No resource quota.

No resource limits.
==> Linting charts/cuda-gridder/
[INFO] Chart.yaml: icon is recommended

1 chart(s) linted, no failures
job.batch/gridder-cuda-gridder-test created

$ kubectl get all
NAMESPACE     NAME                                            READY   STATUS      RESTARTS   AGE    IP                NODE       NOMINATED
 NODE   READINESS GATES
default       pod/gridder-cuda-gridder-test-zsqw6             0/1     Completed   0          17m    192.168.150.144   minikube   <none>
        <none>
...
$ $ make logs
---------------------------------------------------
Logs for pod/gridder-cuda-gridder-test-zsqw6
kubectl -n default logs pod/gridder-cuda-gridder-test-zsqw6
kubectl -n default get pod/gridder-cuda-gridder-test-zsqw6 -o jsonpath={.spec.initContainers[*].name}
---------------------------------------------------
Main Pod logs for pod/gridder-cuda-gridder-test-zsqw6
---------------------------------------------------
Container: gridder
>>> UPDATE: Determining memory requirements for convolution kernels...
>>> UPDATE: Requirements analysis complete...
>>> UPDATE: Allocating resources for grid and convolution kernels...
>>> UPDATE: Resource allocation successful...
>>> UPDATE: Loading kernels...
>>> UPDATE: Loading kernels complete...
>>> UPDATE: Loading visibilities...
>>> UPDATE: Loading visibilities complete...
>>> UPDATE: LOADING IN CONVOLUTION KERNEL SAMPLES...
>>> UPDATE: Performing W-Projection based convolutional gridding...
>>> INFO: Using 1954 blocks, 1024 threads, for 2000000 visibilities...
>>> UPDATE: GPU accelerated gridding completed in 162.384644 milliseconds...
UPDATE >>> IGNORING iFFT and CC...
UPDATE >>> COPYING GRID BACK TO CPU....
PIPELINE COMPLETE....
>>> UPDATE: Gridding complete...
>>> UPDATE: Saving grid to file...
>>> UPDATE: Save successful...
>>> UPDATE: Cleaning up allocated resources...
>>> UPDATE: Cleaning complete, exiting...
---------------------------------------------------
---------------------------------------------------
```

Cleanup with `make delete`
